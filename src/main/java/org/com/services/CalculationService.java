package org.com.services;

import java.util.List;
import java.util.Optional;

import org.com.entities.GroupWeightage;
import org.com.entities.Ratings;
import org.com.repositories.GroupWeightages;
import org.com.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculationService {
	@Autowired
	RatingRepository ratingRepository;
	@Autowired
	GroupWeightages groupWeightageRepository;
	public Double weightageof(String group) {
		Optional<GroupWeightage> groupList=groupWeightageRepository.findAllById(group);
		List<Ratings>ratingList=ratingRepository.findAll();
		System.out.println("group list"+groupList);
		System.out.println("rating list"+ratingList);
		
		return 1.2;
	}
}
