package org.com.repositories;

import org.com.entities.Ratings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository extends JpaRepository<Ratings, String>{

}
