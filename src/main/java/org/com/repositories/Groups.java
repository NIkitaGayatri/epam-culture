package org.com.repositories;

import org.com.entities.AssociateGroups;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Groups extends JpaRepository<AssociateGroups,String>{

}
