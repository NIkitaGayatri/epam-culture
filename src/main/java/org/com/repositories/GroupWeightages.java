package org.com.repositories;

import java.util.Optional;

import org.com.entities.GroupWeightage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GroupWeightages extends JpaRepository<GroupWeightage, String>{
	@Query(value="SELECT * FROM group_weightage WHERE group_id=?1",nativeQuery = true)
	public Optional<GroupWeightage> findAllById(String groupId);
}
