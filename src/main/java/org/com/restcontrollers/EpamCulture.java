package org.com.restcontrollers;

import org.com.services.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EpamCulture {
	@Autowired 
	CalculationService calculationService;
	@GetMapping("/hello")
	public ResponseEntity<Double> getCalculation(){
		return new ResponseEntity<>(calculationService.weightageof("G1"),HttpStatus.OK);
	}

}
