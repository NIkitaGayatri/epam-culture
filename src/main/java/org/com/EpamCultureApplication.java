package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpamCultureApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpamCultureApplication.class, args);
	}

}
