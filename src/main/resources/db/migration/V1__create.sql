CREATE TABLE associate_groups (
  level_id VARCHAR(45) NOT NULL ,
  group_Id VARCHAR(45) NOT NULL,
  PRIMARY KEY (level_id)
);

CREATE TABLE ratings(
	rating_type VARCHAR(45) NOT NULL ,
	rating_weight INT(11) NOT NULL,
    rating DOUBLE NOT NULL,
    PRIMARY KEY (rating_type)
);
CREATE TABLE group_weightage(
	group_id VARCHAR(45) NOT NULL ,
	weightage INT(11) NOT NULL,
    behaviour VARCHAR(45) NOT NULL
);