INSERT INTO associate_groups (level_id, group_Id) VALUES ('A1', 'G1');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('A2', 'G1');
INSERT INTO associate_groups (level_id,group_Id) VALUES ('A3', 'G1');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('A4', 'G2');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('B1', 'G2');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('B2', 'G2');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('B3', 'G3');
INSERT INTO associate_groups (level_id, group_Id) VALUES ('B4', 'G3');




INSERT INTO ratings (rating_type, rating_weight, rating) VALUES ('self-review', '20', '4');
INSERT INTO ratings (rating_type, rating_weight, rating) VALUES ('manager-review', '30', '3');
INSERT INTO ratings (rating_type, rating_weight, rating) VALUES ('peer-review', '25', '3');
INSERT INTO ratings (rating_type, rating_weight, rating) VALUES ('practicehead-eview', '25', '4');



INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '15', 'B1');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '10', 'B2');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '10', 'B3');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '15', 'B4');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '5', 'B5');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '20', 'B6');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '10', 'B7');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '5', 'B8');
INSERT INTO group_weightage (group_id, weightage, behaviour) VALUES ('G1', '10', 'B9');
